<?php

declare(strict_types=1);

namespace Local\Core\SEO;

/**
 * Класс для предварительной обработки URL адресов (редиректы)
 */
class URL
{
    private static $r;

    /**
     * Запуск на событии
     *
     * @return void
     */
    public static function onPageStart()
    {
        if (self::checkExcludes()) {
            return;
        }
        self::isCurrentUrlCorrect();
    }

    /**
     * Проверка исключений в URL
     *
     * @return bool|void
     */
    private static function checkExcludes()
    {
        $arExcludes = [
            '/bitrix/',
            '/ajax/',
            '/filter/',
            'form',
            '/search/',
            'PAGEN_',
            'clear_cache=',
            'bitrix_include_areas=',
            'arrfilter',
            'ajax=',
            'utm_source=',
            'utm_medium=',
            'utm_term=',
            'utm_campaign=',
        ];

        foreach ($arExcludes as $strExclude) {
            if (strpos($_SERVER['REQUEST_URI'], $strExclude) !== false) {
                return true;
            }
        }
    }

    /**
     * Проверка корректности текущего URL с помощью приведения и шаблонов
     */
    private static function isCurrentUrlCorrect(): void
    {
        $aReplace = [
            '/index.php',
            /*
            '/-',
            '-/',
            '/--',
            '--/',
            '/---',
            '---/',
            '/----',
            '----/',
            '/-----',
            '-----/',
            '////////////////',
            '///////////////',
            '//////////////',
            '/////////////',
            '////////////',
            '///////////',
            '//////////',
            '////////',
            '///////',
            '//////',
            '/////',
            '////',
            '///',
            '//',
            '/',
            */
        ];

        $sUri = $_SERVER['REQUEST_URI'];
        //$sUri = strtolower(str_replace('_', '-', $sUri));
        $sUri = str_replace($aReplace, '/', $sUri);

        if ($sUri != $_SERVER['REQUEST_URI']) {
            self::redirect($sUri);
        }
    }

    /**
     *  Если преобразованный URL отличается, 301 редирект
     *
     * @param string $sUri
     * @return void
     */
    private static function redirect(string $sUri): void
    {
        $sUrl = self::getProtocol() . '://' . $_SERVER['SERVER_NAME'] . $sUri;

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $sUrl);
        exit;
    }

    /**
     * Функция
     */
    private static function getProtocol(): string
    {
        if ($_SERVER['REDIRECT_HTTPS'] == 'on') {
            return 'https';
        } else {
            return 'http';
        }
    }
}
